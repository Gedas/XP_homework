"""Tests for the tracker"""
from datetime import timedelta
from time import sleep
from django.test import TestCase, LiveServerTestCase
from django.core.exceptions import ValidationError
from django.db.models.deletion import ProtectedError
from django.db.utils import IntegrityError
from django.contrib import messages

# change "firefox" to "chrome" to use chrome and chromeengine instead
from selenium.webdriver.firefox.webdriver import WebDriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys
import selenium.webdriver.support.ui as ui
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import WebDriverException
from selenium.webdriver.common.by import By

from .models import Story, Developer, Task, TimeLine
from .forms import StoryForm, TaskForm, TimeForm

# Model tests


class ModelTests(TestCase):

    def test_model_structure(self):
        # initialize variables
        story = Story(name="test_story", description="story_description",
                      estimated_time=timedelta(days=1, hours=1, minutes=1))
        developer = Developer(name="test_dev", description="dev_description")
        task = Task(name="test_task", description="task_description",
                    estimated_time=timedelta(days=0.5, hours=1, minutes=1),
                    iteration=1, story=story, developer=developer)
        time_line = TimeLine(name="test_line", description="line_description",
                             time_spent=timedelta(days=0.5, hours=0.5,
                                                  minutes=0),
                             task=task, developer=developer)

        # test
        self.assertIs(task.story, story)
        self.assertIs(task.developer, developer)
        self.assertIs(time_line.task, task)
        self.assertIs(time_line.developer, developer)
        self.assertIs(time_line.task.story, story)

    def test_model_empty_fields(self):
        with self.assertRaises(ValidationError):
            Story().save()
            Story().full_clean()

        story = Story(name="test_story")
        story.full_clean()
        story.save()

        with self.assertRaises(ValidationError):
            task = Task(story=story)
            task.full_clean()

        with self.assertRaises(ValidationError):
            Developer().full_clean()
        dev = Developer(name="dev")
        dev.full_clean()
        dev.save()

        task = Task(name="test_task", story=story)
        task.full_clean()
        task.save()

        with self.assertRaises(ValidationError):
            line = TimeLine(name="test_line", task=task)
            line.full_clean()
        line = TimeLine(name="test_line", task=task, developer=dev)
        line.full_clean()
        line.save()

    def test_model_task_null_references(self):
        with self.assertRaises(IntegrityError):
            task = Task(name="test_task")
            task.full_clean()
            task.save()

    def test_model_timeline_nullref(self):
        dev = Developer(name="dev")
        dev.save()
        with self.assertRaises(IntegrityError):
            line = TimeLine(name="test_line", developer=dev)
            line.full_clean()
            line.save()

    def test_model_positive_duration(self):
        with self.assertRaises(ValidationError):
            Story(name="test_story", description="story_description",
                  estimated_time=timedelta(days=-0.0001)).full_clean()
        story = Story(name="test_story", description="story_description",
                      estimated_time=timedelta(days=1, hours=1, minutes=1))
        with self.assertRaises(ValidationError):
            Task(name="test_task", description="task_description",
                 estimated_time=timedelta(days=-0.0001), iteration=1,
                 story=story).full_clean()
        task = Task(name="test_task", description="task_description",
                    estimated_time=timedelta(days=0.5), iteration=1,
                    story=story)
        with self.assertRaises(ValidationError):
            TimeLine(name="test_line", description="line_description",
                     time_spent=timedelta(days=-0.0001),
                     task=task).full_clean()

    def test_developer_deletion(self):
        story = Story(name="test_story", description="story_description",
                      estimated_time=timedelta(days=1, hours=1, minutes=1))
        story.save()
        developer = Developer(name="test_dev", description="dev_description")
        developer.save()
        developer2 = Developer(name="test_dev2", description="dev_description")
        developer2.save()

        self.assertTrue(developer.can_delete())
        self.assertTrue(developer2.can_delete())

        task = Task(name="test_task", description="task_description",
                    estimated_time=timedelta(days=0.5, hours=1, minutes=1),
                    iteration=1, story=story, developer=developer)
        task.save()

        self.assertFalse(developer.can_delete())
        self.assertTrue(developer2.can_delete())

        time_line = TimeLine(name="test_line", description="line_description",
                             time_spent=timedelta(days=0.5, hours=0.5,
                                                  minutes=0),
                             task=task, developer=developer2)
        time_line.save()
        self.assertFalse(developer.can_delete())
        self.assertFalse(developer2.can_delete())

        with self.assertRaises(ProtectedError):
            developer.delete()
        with self.assertRaises(ProtectedError):
            developer2.delete()

        time_line.delete()
        self.assertTrue(developer2.can_delete())
        task.delete()
        self.assertTrue(developer.can_delete())
        developer.delete()
        developer2.delete()

    def test_model_time_logic(self):
        story = Story(name="test_story", description="story_description",
                      estimated_time=timedelta(1))
        story.save()
        developer = Developer(name="test_dev", description="dev_description")
        developer.save()
        task = Task(name="test_task", description="task_description",
                    estimated_time=timedelta(0.5), iteration=1, story=story,
                    developer=developer)
        task.save()
        time_line = TimeLine(name="test_line", description="line_description",
                             time_spent=timedelta(0.2), task=task,
                             developer=developer)
        time_line.save()

        # test keys too
        pkey = developer.pk
        del developer
        developer = Developer.objects.get(pk=pkey)
        pkey = task.pk
        del task
        task = Task.objects.get(pk=pkey)

        self.assertEqual(task.time_spent, timedelta(0.2))
        self.assertEqual(story.time_spent, timedelta(0.2))
        self.assertEqual(developer.time_spent, timedelta(0.2))
        self.assertEqual(developer.estimated_time, timedelta(0.5))

        task.estimated_time = timedelta(0.7)
        task.save()
        self.assertEqual(Developer.objects.get(pk=developer.pk).estimated_time,
                         timedelta(0.7))
        task.developer = None
        task.save()
        self.assertEqual(Developer.objects.get(pk=developer.pk).estimated_time,
                         timedelta(0))

        time_line.time_spent = timedelta(0.4)
        time_line.save()
        self.assertEqual(Story.objects.get(pk=story.pk).time_spent,
                         timedelta(0.4))
        self.assertEqual(Task.objects.get(pk=task.pk).time_spent,
                         timedelta(0.4))
        self.assertEqual(Developer.objects.get(pk=developer.pk).time_spent,
                         timedelta(0.4))
        dev2 = Developer(name='dev2', active=False)
        dev2.save()
        time_line.developer = dev2
        story2 = Story(name='story2')
        story2.save()
        task2 = Task(name="bad", story=story2)
        task2.save()
        # current model definition should prevent this, but let's be thorough
        time_line.task = task2
        time_line.save()
        self.assertEqual(Developer.objects.get(pk=developer.pk).time_spent,
                         timedelta(0))
        self.assertEqual(Task.objects.get(pk=task.pk).time_spent, timedelta(0))
        self.assertEqual(Story.objects.get(pk=story.pk).time_spent,
                         timedelta(0))


class FormTests(TestCase):
    def test_form_fields(self):
        self.assertTrue(StoryForm({'name': 'test'}).is_valid())
        self.assertTrue(StoryForm({'name': 'test', 'description': '',
                                   'estimated_time': ''}).is_valid())
        self.assertFalse(StoryForm({}).is_valid())
        self.assertFalse(StoryForm(
            {'name': 'test', 'estimated_time': 'test'}
            ).is_valid())
        self.assertFalse(StoryForm(
            {'name': 'test', 'estimated_time': -1}
            ).is_valid())

        # form validation ignores non-editable fields, i.e. story key.
        # This is tested at model level and view level
        self.assertTrue(TaskForm({'name': 'test'}).is_valid())
        self.assertTrue(TaskForm(
            {'name': 'test', 'description': '', 'estimated_time': '',
             'iteration': '', 'developer': ''}
            ).is_valid())
        self.assertFalse(TaskForm({}).is_valid())
        self.assertFalse(TaskForm(
            {'name': 'test', 'estimated_time': 'test'}
            ).is_valid())
        self.assertFalse(TaskForm(
            {'name': 'test', 'estimated_time': -1}
            ).is_valid())
        self.assertFalse(TaskForm(
            {'name': 'test', 'iteration': 'test'}
            ).is_valid())
        self.assertFalse(TaskForm(
            {'name': 'test', 'developer': 'test'}
            ).is_valid())

        dev = Developer(name='test')
        dev.save()
        self.assertTrue(TimeForm(
            {'name': 'test', 'developer': dev.pk, 'time_spent': 0}
            ).is_valid())
        self.assertTrue(TimeForm(
            {'name': 'test', 'description': '', 'developer': dev.pk,
             'time_spent': 0}
            ).is_valid())
        self.assertFalse(TimeForm({}).is_valid())
        self.assertFalse(TimeForm(
            {'name': 'test', 'developer': dev.pk}
            ).is_valid())
        self.assertFalse(TimeForm(
            {'name': 'test', 'developer': dev.pk, 'time_spent': -1}
            ).is_valid())
        self.assertFalse(TimeForm(
            {'name': 'test', 'time_spent': 0}).is_valid())
        self.assertFalse(TimeForm(
            {'name': 'test', 'developer': 'test', 'time_spent': 1}
            ).is_valid())


class ViewTests(TestCase):
    available_apps = [
        'django.contrib.auth',
        'django.contrib.messages',
    ]

    def test_story_addition(self):
        self.assertFalse(Story.objects.filter(name='test'))
        response = self.client.post('/add_story', {'name': 'test'})
        self.assertEqual(len(Story.objects.filter(name='test')), 1)
        for message in messages.get_messages(response.wsgi_request):
            self.assertEqual(str(message), 'Story created.')

    def test_story_addition_failure(self):
        response = self.client.post('/add_story', {'estimated_time': 'test'})
        self.assertFalse(Story.objects.all())
        for message in messages.get_messages(response.wsgi_request):
            self.assertEqual(str(message), 'Story not created.')

    def test_story_edit(self):
        story = Story(name="test_story")
        story.save()

        response = self.client.post('/story/'+str(story.pk)+'/edit_story',
                                    {'name': 'changed_story'})
        self.assertTrue(Story.objects.filter(name='changed_story'))
        self.assertFalse(Story.objects.filter(name='test_story'))

        for message in messages.get_messages(response.wsgi_request):
            self.assertEqual(str(message), 'Story updated.')

    def test_story_edit_bad_parameters(self):
        story = Story(name="test_story")
        story.save()

        response = self.client.post('/story/'+str(story.pk)+'/edit_story',
                                    {'name': ''})
        self.assertFalse(Story.objects.filter(name=''))
        self.assertTrue(Story.objects.filter(name='test_story'))

        for message in messages.get_messages(response.wsgi_request):
            self.assertEqual(str(message), 'Story not updated.')

    def test_story_edit_nonexistant(self):
        response = self.client.post('/story/1/edit_story', {'name': 'test'})
        self.assertFalse(Story.objects.filter(name='test'))

        for message in messages.get_messages(response.wsgi_request):
            self.assertEqual(str(message), 'Story deleted.')

    def test_task_add(self):
        story = Story(name="test_story")
        story.save()

        response = self.client.post('/story/%s/add_task' % story.pk,
                                    {'name': 'test'})
        self.assertTrue(Task.objects.filter(name='test'))
        for message in messages.get_messages(response.wsgi_request):
            self.assertEqual(str(message), 'Task created.')

    def test_task_add_nonexistant_story(self):
        response = self.client.post('/story/1/add_task', {'name': 'test'})
        self.assertFalse(Task.objects.all())
        for message in messages.get_messages(response.wsgi_request):
            self.assertEqual(str(message), 'Story deleted.')

    def test_task_add_noname(self):
        story = Story(name="test_story")
        story.save()

        response = self.client.post('/story/%s/add_task' % story.pk,
                                    {'name': ''})
        self.assertFalse(Task.objects.all())
        for message in messages.get_messages(response.wsgi_request):
            self.assertEqual(str(message), 'Task not created.')

    def test_task_edit(self):
        story = Story(name="test_story")
        story.save()
        task = Task(story=story, name='test_task')
        task.save()
        developer = Developer(name='test_dev')
        developer.save()

        response = self.client.post(
            '/story/%s/edit_task/%s' % (story.pk, task.pk),
            {'name': 'changed_name', 'estimated_time': '1',
             'iteration': '1', 'developer': developer.pk}
            )
        for message in messages.get_messages(response.wsgi_request):
            self.assertEqual(str(message), 'Task updated.')
        self.assertTrue(Task.objects.filter(name='changed_name'))
        task = Task.objects.filter(name='changed_name')[0]
        self.assertEqual(task.estimated_time, timedelta(0, 1))
        self.assertEqual(task.iteration, 1)
        self.assertEqual(task.developer, developer)

    def test_task_edit_nonexistant(self):
        story = Story(name="test_story")
        story.save()

        response = self.client.post('/story/%s/edit_task/%s' % (story.pk, 1),
                                    {'name': 'changed_name'})
        for message in messages.get_messages(response.wsgi_request):
            self.assertEqual(str(message), 'Task deleted.')
        self.assertFalse(Task.objects.all())

    def test_task_edit_bad_fields(self):
        story = Story(name="test_story")
        story.save()
        task = Task(story=story, name='test_task')
        task.save()

        response = self.client.post(
            '/story/%s/edit_task/%s' % (story.pk, task.pk),
            {'name': 'changed_name', 'estimated_time': '-1'}
            )
        for message in messages.get_messages(response.wsgi_request):
            self.assertEqual(str(message), 'Task not updated.')
        self.assertFalse(Task.objects.filter(name='changed_name'))
        self.assertTrue(Task.objects.filter(name='test_task'))
        self.assertEqual(Task.objects.get(pk=task.pk).estimated_time,
                         timedelta(0))

    def test_task_delete(self):
        story = Story(name="test_story")
        story.save()
        task = Task(story=story, name='test_task')
        task.save()

        self.assertTrue(Task.objects.all())
        response = self.client.post(
            '/story/%s/delete_task/%s' % (story.pk, task.pk), {}
            )
        for message in messages.get_messages(response.wsgi_request):
            self.assertEqual(str(message), 'Task deleted.')
        self.assertFalse(Task.objects.all())

    def test_task_delete_fail(self):
        story = Story(name="test_story")
        story.save()
        task = Task(story=story, name='test_task')
        task.save()

        self.assertTrue(Task.objects.filter(name='test_task'))
        self.client.get(
            '/story/%s/delete_task/%s' % (story.pk, task.pk)
            )
        self.assertTrue(Task.objects.filter(name='test_task'))

    def test_task_delete_nonexistant(self):
        story = Story(name="test_story")
        story.save()
        task = Task(story=story, name='test_task')
        task.save()

        self.assertTrue(Task.objects.filter(name='test_task'))
        response = self.client.post(
            '/story/%s/delete_task/%s' % (story.pk, 2), {}
            )
        for message in messages.get_messages(response.wsgi_request):
            self.assertEqual(str(message), 'Task already deleted.')
        self.assertTrue(Task.objects.filter(name='test_task'))

    def test_lines_edit_nonexistant(self):
        story = Story(name="test_story")
        story.save()
        task = Task(story=story, name='test_task')
        task.save()

        response = self.client.post(
            '/story/%s/update_lines/%s' % (story.pk, 2), {}
            )
        for message in messages.get_messages(response.wsgi_request):
            self.assertEqual(str(message), 'Task deleted.')


class LiveTest(LiveServerTestCase):
    """
    Selenium tests. Use geckoengine and firefox.
    Easily switch to chrome by changing the import statement
    at the beginning of the file.
    """
    @classmethod
    def setUpClass(cls):
        super(LiveTest, cls).setUpClass()

        cls.run_test = True
        try:
            cls.selenium = WebDriver()
        except FileNotFoundError:
            cls.run_test = False
        except WebDriverException:
            cls.run_test = False

        if cls.run_test:
            cls.selenium.implicitly_wait(10)
        else:
            print('WARNING: Selenium tests not running, '
                  'webdriver could not be located or some other reason.')

    @classmethod
    def tearDownClass(cls):
        try:
            cls.selenium.quit()
        except AttributeError:
            pass
        super(LiveTest, cls).tearDownClass()

    def click2(self, element):
        """ActionChains click, no idea why normal one fails in some places"""
        (
            ActionChains(self.selenium)
            .move_to_element(element)
            .click(element)
            .perform()
        )

    def expand_card(self, card_finder_args,
                    next_element_finder_args,
                    card_finder_function=None,
                    next_element_finder_function=None):
        """
        Locates and element based on first finder function and arguments,
        then waits until the second element becomes visible, and returns it.
        """
        if not card_finder_function:
            card_finder_function = self.selenium.find_element_by_xpath
        if not next_element_finder_function:
            next_element_finder_function = self.selenium.find_element_by_xpath

        card = card_finder_function(*card_finder_args)
        self.click2(card)
        element = next_element_finder_function(*next_element_finder_args)
        ui.WebDriverWait(self.selenium, 10).until(EC.visibility_of(element))
        return element

    def test_model_stack(self):
        if not self.run_test:
            return
        wait = ui.WebDriverWait(self.selenium, 10)

        # create a couple of devs first,
        # as developer creation is through admin interface only
        Developer.objects.create(name='Dev', description='Working dev',
                                 active=True)
        Developer.objects.create(name='Ded', description='Dead dev',
                                 active=False)

        # load page
        self.selenium.get('%s%s' % (self.live_server_url, '/'))
        # add_story
        story_form = self.expand_card(("//div[@id='add_story']/a[1]",),
                                      ("//input[@id='id_name']",))
        story_form.send_keys("test_story1", Keys.TAB, 'test_story_description',
                             Keys.TAB, '1 1:1', Keys.TAB, Keys.RETURN)
        # go to the story
        story_link = self.selenium.find_element_by_link_text("test_story1")
        self.click2(story_link)

        sleep(0.1)

        task_field = self.expand_card(("//div[@id='add_task']/h5[1]/a[1]",),
                                      ("//div[@id='collapseOne']//descendant::\
                                       input[@id='id_name']",))
        # check dev choices
        dev_list = ui.Select(task_field.find_element_by_xpath(
            'ancestor::form//descendant::select[@id="id_developer"]'
            )).options
        for dev in dev_list:
            if dev.text == '-'*9:
                continue
            self.assertTrue(Developer.objects.filter(name=dev.text)[0].active)
        # add task
        task_field.send_keys("test_task1", Keys.TAB, 'test_task_description',
                             Keys.TAB, '2', Keys.TAB, Keys.ARROW_DOWN,
                             Keys.TAB, '1 1:1', Keys.TAB, Keys.RETURN)

        # add time lines
        line_field = self.selenium.find_element_by_id(
            'id_timeline_set-0-name'
            )
        line_field.send_keys('test_line1', Keys.TAB, 'test1', Keys.TAB,
                             Keys.ARROW_DOWN, Keys.TAB, '1:1', Keys.TAB,
                             Keys.TAB, 'test_line2', Keys.TAB, Keys.TAB,
                             Keys.ARROW_DOWN, Keys.TAB, '99:99', Keys.TAB,
                             Keys.TAB, Keys.RETURN)
        # check line dev choices
        sleep(0.1)  # hopefully enough for page to start refreshing
        task_form = self.selenium.find_element_by_id('collapse1')
        task_field = task_form.find_element_by_id('id_name')
        wait.until(EC.visibility_of(task_field))
        dev_list = ui.Select(task_form.find_element_by_id(
            'id_timeline_set-0-developer'
            ))
        for dev in dev_list.options:
            if dev.text == '-'*9:
                continue
            self.assertTrue(Developer.objects.filter(name=dev.text)[0].active)

        # check dev page
        self.selenium.get('%s%s' % (self.live_server_url, '/developers'))
        dev_link = self.selenium.find_element_by_link_text("Dev")
        self.assertEqual(dev_link.find_element_by_xpath(
            'parent::th/parent::tr/th[3]/a[1]'
            ).text, '1:41:40')
        self.assertEqual(dev_link.find_element_by_xpath(
            'parent::th/parent::tr/th[2]/a[1]'
            ).text, '1 day, 0:01:01')
        self.click2(dev_link)

        # expand the assigned tasks card and follow link
        wait.until(EC.presence_of_element_located((By.ID, 'assigned_tasks')))
        task_link = self.expand_card(
            ("//div[@id='assigned_tasks']/h5[1]/a[1]",), ("test_task1",),
            next_element_finder_function=self.selenium.
            find_element_by_link_text)
        self.click2(task_link)

        # modify time items
        line_field = self.selenium.find_element_by_id(
            'id_timeline_set-0-name'
            )
        line_field.clear()
        line_field.send_keys('test_line_item1')
        # mark second line for deletion
        self.selenium.find_element_by_id('id_timeline_set-1-DELETE').click()
        # submit
        line_field.find_element_by_xpath(
            'parent::div/parent::form/input[@type="submit"]'
            ).click()

        sleep(1)  # so database catches up

        # check correctness in database
        story = Story.objects.filter(name='test_story1')[0]
        task = Task.objects.filter(name='test_task1')[0]
        self.assertFalse(TimeLine.objects.filter(name='test_line1'))
        self.assertFalse(TimeLine.objects.filter(name='test_line2'))
        line = TimeLine.objects.filter(name='test_line_item1')[0]
        dev = Developer.objects.filter(active=True)[0]

        self.assertEqual(story.description, 'test_story_description')
        self.assertEqual(story.estimated_time,
                         timedelta(days=1, minutes=1, seconds=1))
        self.assertEqual(story.pk, task.story.pk)
        self.assertEqual(task.description, 'test_task_description')
        self.assertEqual(task.iteration, 2)
        self.assertEqual(task.developer.pk, dev.pk)
        self.assertEqual(task.estimated_time,
                         timedelta(days=1, minutes=1, seconds=1))
        self.assertEqual(task.pk, line.task.pk)
        self.assertEqual(line.description, 'test1')
        self.assertEqual(line.developer.pk, dev.pk)
        self.assertEqual(line.time_spent, timedelta(minutes=1, seconds=1))

        # delete task
        task_delete = self.expand_card(("//*[@id='task-delete1']",),
                                       ('//input[@value="Delete task"]',))
        task_delete.click()

        sleep(1)  # so database catches up
        self.assertFalse(Task.objects.filter(name='test_task1'))
        self.assertFalse(Task.objects.filter(story=story.pk))
        self.assertFalse(Task.objects.all())
        self.assertFalse(TimeLine.objects.all())
