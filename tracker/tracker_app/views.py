"""Django views for the tracker"""
from django.shortcuts import get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.urls import reverse
from django.contrib import messages

from .models import Story, Developer, Task
from .forms import StoryForm, TaskForm, TimeFormSet

# Create your views here.


def stories(request):
    stories_list = Story.objects.all()
    template = loader.get_template('tracker_app/stories.html')
    context = {
        'stories_list': [
            (row.name, row.estimated_time, row.time_spent, row.pk)
            for row in stories_list],
        'stories_header': ('Story', 'Time Estimated', 'Time Spent'),
        'story_form': StoryForm(),
    }
    return HttpResponse(template.render(context, request))


def add_story(request):
    if request.method == 'POST':
        form = StoryForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'Story created.')
        else:
            messages.warning(request, 'Story not created.')
    return HttpResponseRedirect(reverse('stories'))


def story(request, pk, task_pk=None):
    # get story
    try:
        story = Story.objects.get(pk=pk)
    except Story.DoesNotExist:
        messages.warning(request, 'Story deleted.')
        return HttpResponseRedirect(reverse('stories'))

    # if task pk is set, check if this task exists and belongs to this story,
    # else ignore it
    if task_pk:
        try:
            if Task.objects.get(pk=task_pk).story != story:
                task_pk = None
        except Task.DoesNotExist:
            task_pk = None

    # since the template cannot figure out equality between different objects..
    if task_pk:
        task_pk = Task.objects.get(pk=task_pk).pk

    template = loader.get_template('tracker_app/story.html')
    story_form = StoryForm(instance=story)
    task_set = Task.objects.filter(story=story)
    task_list = [
        {
            'task': task,
            'task_form': TaskForm(instance=task),
            'line_set': TimeFormSet(instance=task),
            }
        for task in task_set
        ]
    context = {
        'story': story,
        'story_form': story_form,
        'task_form': TaskForm(),
        'task_list': task_list,
        'task_pk': task_pk,
    }
    return HttpResponse(template.render(context, request))


def edit_story(request, pk):
    try:
        story = Story.objects.get(pk=pk)
    except Story.DoesNotExist:
        messages.warning(request, 'Story deleted.')
        return HttpResponseRedirect(reverse('stories'))
    if request.method == 'POST':
        form = StoryForm(request.POST, instance=story)
        if form.is_valid():
            form.save()
            messages.success(request, 'Story updated.')
        else:
            messages.warning(request, 'Story not updated.')
    return HttpResponseRedirect(reverse('story', kwargs={'pk': pk}))


def add_task(request, pk):
    try:
        story = Story.objects.get(pk=pk)
    except Story.DoesNotExist:
        messages.warning(request, 'Story deleted.')
        return HttpResponseRedirect(reverse('stories'))
    if request.method == 'POST':
        form = TaskForm(request.POST)
        form.instance.story = story
        if form.is_valid():
            form.save()
            messages.success(request, 'Task created.')
            return HttpResponseRedirect(
                reverse('story',
                        kwargs={'pk': pk, 'task_pk': form.instance.pk}
                        ))
        else:
            messages.warning(request, 'Task not created.')
    return HttpResponseRedirect(reverse('story', kwargs={'pk': pk}))


def edit_task(request, story_pk, task_pk):
    try:
        task = Task.objects.get(pk=task_pk)
    except Task.DoesNotExist:
        messages.warning(request, 'Task deleted.')
        return HttpResponseRedirect(reverse('story', kwargs={'pk': story_pk}))
    if request.method == 'POST':
        form = TaskForm(request.POST, instance=task)
        if form.is_valid():
            form.save()
            messages.success(request, 'Task updated.')
        else:
            messages.warning(request, 'Task not updated.')
    return HttpResponseRedirect(
        reverse('story', kwargs={'pk': story_pk, 'task_pk': task_pk})
        )


def delete_task(request, story_pk, task_pk):
    if request.method == 'POST':
        try:
            task = Task.objects.get(pk=task_pk)
            task.delete()
            messages.success(request, 'Task deleted.')
        except Task.DoesNotExist:
            messages.info(request, 'Task already deleted.')
    return HttpResponseRedirect(reverse('story', kwargs={'pk': story_pk}))


def update_lines(request, story_pk, task_pk):
    try:
        task = Task.objects.get(pk=task_pk)
    except Task.DoesNotExist:
        messages.warning(request, 'Task deleted.')
        return HttpResponseRedirect(reverse('story', kwargs={'pk': story_pk}))
    if request.method == 'POST':
        form = TimeFormSet(request.POST, instance=task)
        if form.is_valid():
            form.save()
            messages.success(request, 'Lines updated.')
        else:
            messages.warning(request, 'Lines not updated.')
    return HttpResponseRedirect(reverse('story', kwargs={'pk': story_pk,
                                                         'task_pk': task_pk}))


def developers(request):
    developers_list = Developer.objects.all()
    template = loader.get_template('tracker_app/developers.html')
    context = {
        'developers_list': [
            (row.name, row.estimated_time, row.time_spent, row.active, row.pk)
            for row in developers_list],
        'developers_header': ('Name',
                              'Time Estimated',
                              'Time Spent',
                              'Active'),
    }
    return HttpResponse(template.render(context, request))


def developer(request, pk):
    dev = get_object_or_404(Developer, pk=pk)
    template = loader.get_template('tracker_app/developer.html')
    context = {
        'developer': dev,
        'task_list': [(row.story.name,
                       row.name,
                       row.iteration,
                       row.estimated_time,
                       row.time_spent,
                       '%s/%s' % (row.story.pk, row.pk)
                       ) for row in Task.objects.filter(developer=dev)],
        'task_header': ('Story',
                        'Task',
                        'Iteration',
                        'Estimated time',
                        'Time spent'),
    }
    return HttpResponse(template.render(context, request))
