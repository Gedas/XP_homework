from django.contrib import admin
from .models import Developer, Story, Task, TimeLine

# Register your models here.

admin.register(Developer, Story, Task, TimeLine)(admin.ModelAdmin)
