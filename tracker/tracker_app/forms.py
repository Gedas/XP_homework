"""Module containing django forms for our models"""
from django.forms import ModelForm, TextInput, Textarea
from django.forms import inlineformset_factory
from django.db.models import Q
from .models import Story, Developer, Task, TimeLine

DURATION_FIELD_ATTRIBUTES = {
    'pattern': ('^((\d+) (days?, )?)?(((\d+):)(?=\d+:\d+))?'
                '((\d+):)?(\d+)(\.(\d{1,6})\d{0,6})?$'),
    'title': 'Recommended time format is "days hours:minutes:seconds"'
}


class StoryForm(ModelForm):
    class Meta:
        model = Story
        fields = ['name', 'description', 'estimated_time']
        widgets = {
            'description': Textarea(attrs={'rows': 4}),
            'estimated_time': TextInput(attrs=DURATION_FIELD_ATTRIBUTES)
        }


class ModelWithDevForm(ModelForm):
    """
    Abstract class setting the dev queryset to only select from active ones,
    and the previously selected one.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # only active developers can be selected,
        # but if an inactive is already selected, include it in options
        if ((self.instance and
             hasattr(self.instance, 'developer') and
             self.instance.developer)):
            self.fields['developer'].queryset = Developer.objects.filter(
                Q(active=True) | Q(pk=self.instance.developer.pk)
                )
        else:
            self.fields['developer'].queryset = Developer.objects.filter(
                active=True
                )

    class Meta:
        abstract = True


class TaskForm(ModelWithDevForm):
    class Meta:
        model = Task
        fields = ['name',
                  'description',
                  'iteration',
                  'developer',
                  'estimated_time'
                  ]
        widgets = {
            'description': Textarea(attrs={'rows': 4}),
            'estimated_time': TextInput(attrs=DURATION_FIELD_ATTRIBUTES)
        }


class TimeForm(ModelWithDevForm):
    class Meta:
        model = TimeLine
        fields = ['name', 'description', 'developer', 'time_spent']
        time_spent_attr = {'size': 9}
        time_spent_attr.update(DURATION_FIELD_ATTRIBUTES)
        widgets = {
            'time_spent': TextInput(attrs=time_spent_attr)
        }

TimeFormSet = inlineformset_factory(Task, TimeLine, TimeForm, extra=2)
