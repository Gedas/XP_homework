"""Django models for the tracker"""
from datetime import timedelta
from django.db import models
from django.core.validators import MinValueValidator, MinLengthValidator


# Create your models here.

MAX_NAME_LENGTH = 200
MAX_DESCRIPTION_LENGTH = 2000


class Item(models.Model):
    """An abstract class which tracker items use, containing generic fields."""
    name = models.CharField(
        max_length=MAX_NAME_LENGTH,
        validators=[MinLengthValidator(1, message="Items must have a name.")])
    description = models.CharField(max_length=MAX_DESCRIPTION_LENGTH,
                                   blank=True)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True

    def __str__(self):
        return self.name


class Developer(Item):
    """A developer."""
    active = models.BooleanField(default=True)

    @property
    def estimated_time(self):
        return sum([task.estimated_time for task in
                    Task.objects.filter(developer=self.pk)],
                   timedelta(0))

    @property
    def time_spent(self):
        return sum([line.time_spent for line in
                    TimeLine.objects.filter(developer=self.pk)],
                   timedelta(0))

    def can_delete(self):
        """Check if this developer can be deleted"""
        return not (Task.objects.filter(developer=self.pk) or
                    TimeLine.objects.filter(developer=self.pk))


class Story(Item):
    """A story item."""
    estimated_time = models.DurationField(
        default=timedelta(0),
        blank=True,
        null=True,
        validators=[MinValueValidator(timedelta(0),
                                      message="Value must be non-negative.")])

    @property
    def time_spent(self):
        return sum([task.time_spent for task in
                    Task.objects.filter(story=self.pk)],
                   timedelta(0))


class Task(Item):
    """A single task assigned to a story."""
    story = models.ForeignKey(Story, on_delete=models.CASCADE, editable=False)
    estimated_time = models.DurationField(
        default=timedelta(0),
        blank=True,
        null=True,
        validators=[MinValueValidator(timedelta(0),
                                      message="Value must be non-negative.")])
    iteration = models.IntegerField(default=0, blank=True)
    developer = models.ForeignKey(
        Developer,
        on_delete=models.PROTECT,
        blank=True,
        null=True)

    @property
    def time_spent(self):
        return sum([line.time_spent for line in
                    TimeLine.objects.filter(task=self.pk)],
                   timedelta(0))


class TimeLine(Item):
    """A single line item for tracking time spent on a task."""
    task = models.ForeignKey(Task, on_delete=models.CASCADE, editable=False)
    time_spent = models.DurationField(
        default=timedelta(0),
        validators=[MinValueValidator(timedelta(0),
                                      message="Value must be non-negative.")]
        )
    developer = models.ForeignKey(Developer, on_delete=models.PROTECT)
