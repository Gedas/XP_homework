"""
Definition of urls for tracker.
"""

from django.conf.urls import include, url

import tracker_app.views

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

app_name = 'tracker_app'

urlpatterns = [
    # Examples:
    # url(r'^$', tracker.views.home, name='home'),
    # url(r'^tracker/', include('tracker.tracker.urls'),
    url(r'^$', tracker_app.views.stories, name='stories'),
    url(r'^add_story$', tracker_app.views.add_story, name='add_story'),
    url(r'^story/(?P<pk>[0-9]+)$', tracker_app.views.story, name='story'),
    url(r'^story/(?P<pk>[0-9]+)/(?P<task_pk>[0-9]+)$',
        tracker_app.views.story, name='story'),
    url(r'^story/(?P<pk>[0-9]+)/edit_story$',
        tracker_app.views.edit_story, name='edit_story'),
    url(r'^story/(?P<pk>[0-9]+)/add_task$',
        tracker_app.views.add_task, name='add_task'),
    url(r'^story/(?P<story_pk>[0-9]+)/edit_task/(?P<task_pk>[0-9]+)$',
        tracker_app.views.edit_task, name='edit_task'),
    url(r'^story/(?P<story_pk>[0-9]+)/delete_task/(?P<task_pk>[0-9]+)$',
        tracker_app.views.delete_task, name='delete_task'),
    url(r'^story/(?P<story_pk>[0-9]+)/update_lines/(?P<task_pk>[0-9]+)$',
        tracker_app.views.update_lines, name='update_lines'),
    url(r'^developers$', tracker_app.views.developers, name='developers'),
    url(r'^developer/(?P<pk>[0-9]+)$',
        tracker_app.views.developer, name='developer'),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls), name='admin'),
]
